'use strict';

const express = require('express');
const app = express();
const request = require('request');
const MongoClient = require('mongodb').MongoClient;

let url = 'mongodb://localhost:27017/hillwah';
let freeURI = 'http://freegeoip.net/json/';

app.get('/', function (req, res) {
    res.send('Begin here!');
});

app.get('/ip/:ipAddr', function (req, res) {
    let ipAddr = req.params.ipAddr;
    let rsData = {};

    console.log(ipAddr);
    MongoClient.connect(url, function (err, db) {
        db.collection('ipData').find({
            ip: ipAddr
        }).each(function (err, doc) {
            if (doc) {
                console.dir(doc);
                res.json( { country: doc.country_name } );
            } else {
                request(
                    {
                        url: freeURI + ipAddr,
                        method: "GET"
                    }, function (error, response, body) {
                        let statusCode = response.statusCode;
                        console.log("Status code:", statusCode);
                        if (error) {
                            console.log("error: ", error);
                        }

                        if (statusCode != 200) {
                            res.sendStatus(statusCode);
                        } else {
                            console.log('reponse headers: ', response.headers)
                            console.log('geoip body: ', body);

                            let ipData = JSON.parse(body);
                            try {
                                db.collection('ipData').insertOne(ipData);
                                res.json( { country: ipData.country_name } );
                            } catch (e) {
                                console.log(e);
                            }
                        }
                    }
                )
            }
        })
    })
});

app.listen(3033, function () {
    console.log('New magic happend with 3033...');
});
